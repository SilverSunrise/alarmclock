package kinect.pro.alarmclock.utils

import android.view.View

fun changeVisibilityView(view: View) {

    if (view.alpha == 0f)
        view.animate().alpha(1f).duration = animateTime
    else {
        view.animate().alpha(0f).duration = animateTime
    }

}







