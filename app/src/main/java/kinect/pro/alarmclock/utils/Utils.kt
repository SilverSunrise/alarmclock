package kinect.pro.alarmclock.utils

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import kinect.pro.alarmclock.FragmentAlarmList
import kinect.pro.alarmclock.R

// function to the FragmentTransaction which accepts a Lambda with Receiver as an argument.
// first argument ID Fragment Holder, second Fragment.class
inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {

    val fragmentTransaction = beginTransaction()
    fragmentTransaction.func()
    fragmentTransaction.addToBackStack(null)
    fragmentTransaction.commit()
}

fun AppCompatActivity.changeToStartFragment() {
    val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
    transaction.replace(R.id.fragment_holder, FragmentAlarmList())
    transaction.commit()
}

fun AppCompatActivity.replaceFragment(frameId: Int, fragment: Fragment) {
    supportFragmentManager.inTransaction { replace(frameId, fragment) }
}

fun AppCompatActivity.addFragment(frameId: Int, fragment: Fragment) {
    supportFragmentManager.inTransaction { add(frameId, fragment) }
}

fun AppCompatActivity.isBackStackNull() {
    val backStackEntryCount = supportFragmentManager.backStackEntryCount
    if (backStackEntryCount >= 1) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

    } else {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)
    }
}

fun AppCompatActivity.clearBackStack() {
    supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
}






