package kinect.pro.alarmclock

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kinect.pro.alarmclock.utils.isBackStackNull
import kinect.pro.alarmclock.utils.replaceFragment
import kotlinx.android.synthetic.main.fragment_new_alarm.view.*


class FragmentNewAlarm : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //init
        (activity as MainActivity).isBackStackNull()

        val view: View = inflater.inflate(R.layout.fragment_new_alarm, container, false)

        //init title
        activity!!.title = "New alarm"

        //init listener
        initListener(view)
        return view
    }

    private fun initListener(view: View) {
        view.btnShowCongratulation.setOnClickListener {
            (activity as MainActivity).replaceFragment(
                R.id.fragment_holder,
                FragmentCongratulation()
            )
        }
    }
}