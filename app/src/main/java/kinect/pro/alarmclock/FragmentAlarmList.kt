package kinect.pro.alarmclock

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import kinect.pro.alarmclock.utils.changeVisibilityView
import kinect.pro.alarmclock.utils.isBackStackNull
import kinect.pro.alarmclock.utils.replaceFragment
import kotlinx.android.synthetic.main.fragment_alarm_list.*
import kotlinx.android.synthetic.main.fragment_alarm_list.view.*

class FragmentAlarmList : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        //init
        (activity as MainActivity).isBackStackNull()

        val view: View = inflater.inflate(R.layout.fragment_alarm_list, container, false)

        //init title
        activity!!.title = "You alarms"

        //init Listener
        initListener(view, activity as MainActivity)

        return view
    }

    private fun initListener(
        view: View,
        mainActivity: MainActivity
    ) {
        view.rg_fragment_switcher.setOnCheckedChangeListener { _: RadioGroup, _: Int ->
            isCurrentFragment()
        }
        view.ivPlaceholder.setOnClickListener {
            changeVisibilityView(view.ivPlaceholder)
        }
        view.btnCreateNewAlarm.setOnClickListener {
            mainActivity.replaceFragment(R.id.fragment_holder, FragmentNewAlarm())
        }
    }

    private fun isCurrentFragment() {
        when (rg_fragment_switcher.checkedRadioButtonId) {
            R.id.rb_active_alarm -> Toast.makeText(context, "Active", Toast.LENGTH_SHORT)
                .show()
            R.id.rb_inactive_alarm -> Toast.makeText(context, "Inactive", Toast.LENGTH_SHORT)
                .show()
            R.id.rb_show_all_alarm -> Toast.makeText(context, "All", Toast.LENGTH_SHORT).show()
        }
    }
}
