package kinect.pro.alarmclock

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kinect.pro.alarmclock.utils.clearBackStack
import kinect.pro.alarmclock.utils.isBackStackNull
import kinect.pro.alarmclock.utils.replaceFragment
import kotlinx.android.synthetic.main.fragment_congratulation.view.*


class FragmentCongratulation : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //init
        (activity as MainActivity).isBackStackNull()

        val view: View = inflater.inflate(R.layout.fragment_congratulation, container, false)

        //init title
        activity!!.title = "Successfully"

        //init listener
        initListener(view, (activity as MainActivity))
        return view
    }

    private fun initListener(
        view: View,
        mainActivity: MainActivity
    ) {
        view.btnBackToAlarmList.setOnClickListener {
            (activity as MainActivity).replaceFragment(R.id.fragment_holder, FragmentAlarmList())
            mainActivity.clearBackStack()
        }
    }
}
