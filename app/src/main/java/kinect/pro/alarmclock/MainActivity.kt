package kinect.pro.alarmclock

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kinect.pro.alarmclock.utils.changeToStartFragment
import kinect.pro.alarmclock.utils.isBackStackNull

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        // setTheme(R.style.AppTheme)
        
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        changeToStartFragment()
        isBackStackNull()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}
